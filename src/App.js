import React, { Component } from 'react';
import './App.css';

var axios = require('axios');
const coookie_delimiter = "|||";
const API_KEY = "4b2W2nZ0m5dpf1huH3EbGlq78c7HmbIQ";


class Search extends Component {	
	constructor(props) {
		super(props);

		var previous = [];
		var cookie = document.cookie;
		if(cookie!==""){
			previous = cookie.split(coookie_delimiter);
		}
		this.limit = 10;

		this.state = {
			search_text: '',
			gifs: [],
			previous: previous,
			offset: 0,
			count:0,
			total_count: 0
		};
		this.searchTextChanged = this.searchTextChanged.bind(this);
		this.doSearch = this.doSearch.bind(this);
		this.prevSearch = this.prevSearch.bind(this);
		this.prevPage = this.prevPage.bind(this);
		this.nextPage = this.nextPage.bind(this);
		this.searchClick = this.searchClick.bind(this);
		this.inputKeyPressed = this.inputKeyPressed.bind(this);
		this.doSearch();
	}
  
	searchTextChanged(event){
		this.setState({search_text: event.target.value});
	}
  
	prevPage(event){
		var search = this;
		search.setState({offset: Math.max(search.limit-search.state.offset,0)});
		setTimeout(function(){search.doSearch();});
	}
  
	nextPage(event){
		var search = this;
		search.setState({offset: search.limit+search.state.offset});
		setTimeout(function(){search.doSearch();});
	}

	searchClick(){
		var search = this;
		search.setState({offset: 0});
		setTimeout(function(){search.doSearch();});
	}
  
	prevSearch(event){
		var search = this;
		var q = event.target.innerText;
		this.setState({search_text: q});
		setTimeout(function(){search.searchClick();});
	}

	inputKeyPressed(event){
		if(event.key === 'Enter'){
			this.searchClick();
		}
	}
  
	doSearch(){
		var search = this;
	  
		var q = search.state.search_text;
	  
		if(q && q!==""){
			var previous = search.state.previous.slice();
		  
			if(previous.indexOf(q)===-1){
				previous.splice(10,previous.length-10);
				previous.splice(0,0,q);
			}

			this.setState(prevState => ({
				previous: previous
			}));
			
			document.cookie = previous.join(coookie_delimiter);
		}

		q = encodeURIComponent(q);

		var url;
		if(q!==""){
			url = "http://api.giphy.com/v1/gifs/search";
		} else {
			url = "http://api.giphy.com/v1/gifs/trending";
		}
		axios.get(url+"?q="+q+"&api_key="+API_KEY+"&rating=r&limit="+this.limit+"&offset="+this.state.offset)
		.then(function(response){
			search.setState({
				gifs: response.data.data,
				total_count: response.data.pagination.total_count,
				count: response.data.pagination.count,
				offset: response.data.pagination.offset
			});
		});
	}
  
	render() {
					
		var previous_list = [];
		for(var i=0;i<this.state.previous.length;i++){
			var p_text = this.state.previous[i];
			previous_list.push(<span key={i} onClick={this.prevSearch}>{p_text}</span>);
			if(i>9) break;
		}

		var previous_jsx = <div className="previous">{previous_list}</div>;
	
		var header_jsx = <h3>{this.state.search_text!=="" ? this.state.search_text : "Trending"}</h3>;
	
		var gifs_jsx = 	<div className="results">
							{this.state.gifs.map((gif) => <GIF key={gif.slug} gif={gif} />)}
						</div>;
	
		var pagination_jsx = [];
	
		if(this.state.total_count!==undefined){
			if(this.state.offset>0){
				pagination_jsx.push(<button key="prev" onClick={this.prevPage}>Prev Page</button>);
			}
			if(this.state.count>0){
				var from = this.state.offset+1;
				var to = this.state.offset+this.state.count;
				pagination_jsx.push(<span key="info"> GIFs {from} - {to} of {this.state.total_count}</span>);
			}
			if(this.state.offset+this.state.count<this.state.total_count){
				pagination_jsx.push(<button key="next" onClick={this.nextPage}>Next Page</button>);
			}
		}
	
		return (
			<div className="searchContainer">
				<div className="search">
					<input placeholder="Type some keywords to find awesome GIFs" type="text" id="search" value={this.state.search_text} onChange={this.searchTextChanged} onKeyPress={this.inputKeyPressed} />
					<button onClick={this.searchClick}>GO</button>
				</div>
				{previous_jsx}
				{header_jsx}
				{gifs_jsx}
				  <div className="pagination">
					{pagination_jsx}
				  </div>
			</div>
		);
	}
}

class GIF extends Component {
	constructor(props) {
		super(props);
		this.gif = props.gif;
	}
  
	render() {
		return (
			<div className="gif">
				<img src={this.gif.images.original.url} alt={this.gif.title} title={this.gif.title} />
			</div>
		);
	}
}


class App extends Component {
	render() {
		return (
			<div>
				<h1>GIFF-APP</h1>
				<Search />
			</div>
		);
	}
}

export default App;
